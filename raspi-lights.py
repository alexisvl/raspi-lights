#!/usr/bin/env py
# Copyright (C) 2020 Alexis Lockwood.
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <http://www.gnu.org/licenses/>.thon3

import queue
import subprocess
import threading
import time

import RPi.GPIO as GPIO
GPIO.setmode(GPIO.BCM)

RELAY_GPIOS = [14, 15, 4, 17]
LED_GPIO = 27

SCAN_PERIOD = 0.01

# Minimum durations for a medium and long press
MEDIUMPRESS = 0.75
LONGPRESS = 3.00

# Plugs and bulbs
PLUG_P3 = "10.0.12.3" #bookshelf
BULB_L1 = "10.0.12.6" #desk
PLUG_DO = "pyhs100 --plug --ip"
BULB_DO = "pyhs100 --bulb --ip"

ACTIONS = [
    (0, "short", f"{PLUG_DO} {PLUG_P3} on && {BULB_DO} {BULB_L1} on"),
    (0, "med",   f"{PLUG_DO} {PLUG_P3} off && {BULB_DO} {BULB_L1} off"),
]

def setup_gpios():
    for gpio in RELAY_GPIOS:
        GPIO.setup(gpio, GPIO.IN)

    GPIO.setup(LED_GPIO, GPIO.IN, pull_up_down=GPIO.PUD_UP)

class PressEvent:
    def __init__(self, key):
        self.key = key
    def __str__(self):
        return f"PressEvent({self.key})"

class ReleaseEvent:
    def __init__(self, key, dur):
        self.key = key
        self.dur = dur
    def __str__(self):
        return f"ReleaseEvent({self.key}, {self.dur})"

class GpioScanner(threading.Thread):
    def __init__(self):
        super().__init__()
        self.queue = queue.Queue()
        self.stop = False
        self.key_held = None
        self.key_hold_time = None

    def run(self):
        last_relay_gpios = [GPIO.input(i) for i in RELAY_GPIOS]
        last_led_gpio = GPIO.input(LED_GPIO)

        while not self.stop:
            time.sleep(SCAN_PERIOD)

            relay_gpios = [GPIO.input(i) for i in RELAY_GPIOS]
            led_gpio = GPIO.input(LED_GPIO)

            relay_toggles = [i != j for i, j in zip(last_relay_gpios, relay_gpios)]
            led_deassert = (led_gpio and not last_led_gpio)

            last_relay_gpios = relay_gpios
            last_led_gpio = led_gpio

            if led_deassert:
                self.emit_release()

            if sum(relay_toggles) == 1:
                press = relay_toggles.index(True)
                self.emit_press(press)

    def emit_release(self):
        if self.key_held is None:
            return

        dur = time.monotonic() - self.key_hold_time
        self.queue.put(ReleaseEvent(self.key_held, dur))

        self.key_held = None

    def emit_press(self, n):
        self.key_held = n
        self.key_hold_time = time.monotonic()
        self.queue.put(PressEvent(n))

if __name__ == "__main__":
    setup_gpios()
    gs = GpioScanner()
    gs.start()

    while True:
        try:
            e = gs.queue.get()
            if isinstance(e, ReleaseEvent):
                if e.dur >= LONGPRESS:
                    press_type = "long"
                elif e.dur >= MEDIUMPRESS:
                    press_type = "med"
                else:
                    press_type = "short"
                for each_key, each_press, each_cmd in ACTIONS:
                    if each_key == e.key and each_press == press_type:
                        subprocess.Popen(each_cmd, shell=True)
                        break
                else:
                    print(e)
        except KeyboardInterrupt:
            gs.stop = True
            raise
